package br.com.indra.selecao.pedrolima.client.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Component
@ConfigurationProperties("indra")
public class ConfigProperty {

	private String urlApi;

}
