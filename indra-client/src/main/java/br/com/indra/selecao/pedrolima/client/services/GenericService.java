package br.com.indra.selecao.pedrolima.client.services;

import java.util.List;

public interface GenericService<T> {

	void save(T obj);

	T find(long id);

	void delete(long id);

	List<T> list();

}
