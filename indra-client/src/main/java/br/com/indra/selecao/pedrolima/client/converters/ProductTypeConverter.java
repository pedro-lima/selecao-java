package br.com.indra.selecao.pedrolima.client.converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.springframework.stereotype.Component;

import br.com.indra.selecao.pedrolima.client.models.ProductType;

@Component(value = "productTypeConverter")
@FacesConverter(value = "productTypeConverter")
public class ProductTypeConverter implements Converter<ProductType> {

	@Override
	public ProductType getAsObject(FacesContext context, UIComponent component, String value) {
		return ProductType.forValue(value);
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, ProductType value) {
		return value.eventName();
	}

}
