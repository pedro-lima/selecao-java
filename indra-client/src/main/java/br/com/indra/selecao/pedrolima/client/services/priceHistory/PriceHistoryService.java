package br.com.indra.selecao.pedrolima.client.services.priceHistory;

import br.com.indra.selecao.pedrolima.client.models.PriceHistory;
import br.com.indra.selecao.pedrolima.client.services.GenericService;

public interface PriceHistoryService extends GenericService<PriceHistory> {
	
}
