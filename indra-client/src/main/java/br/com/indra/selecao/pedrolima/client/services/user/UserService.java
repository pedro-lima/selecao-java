package br.com.indra.selecao.pedrolima.client.services.user;

import br.com.indra.selecao.pedrolima.client.models.User;
import br.com.indra.selecao.pedrolima.client.services.GenericService;

public interface UserService extends GenericService<User> {

}
