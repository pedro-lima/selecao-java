package br.com.indra.selecao.pedrolima.client.models;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

@Data
public class User implements Serializable {

	private static final long serialVersionUID = 7455203650301166426L;

	private Long id;

	private String name;

	private String email;

	private String password;

	private List<Permission> permissions;

}
