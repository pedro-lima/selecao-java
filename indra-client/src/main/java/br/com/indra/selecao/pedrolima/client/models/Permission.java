package br.com.indra.selecao.pedrolima.client.models;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import lombok.Data;

@Data
public class Permission implements Serializable {

	private static final long serialVersionUID = 8824832148938190759L;

	private Long id;

	@JsonProperty(access = Access.WRITE_ONLY)
	private String description;

	@JsonProperty(access = Access.WRITE_ONLY)
	private String value;

}
