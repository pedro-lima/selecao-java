package br.com.indra.selecao.pedrolima.client.converters;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.springframework.stereotype.Component;

@Component(value = "localDateTimeConverter")
@FacesConverter(value = "localDateTimeConverter")
public class LocalDateTimeConverter implements Converter<LocalDateTime> {

	private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");

	@Override
	public LocalDateTime getAsObject(FacesContext context, UIComponent component, String value) {
		return LocalDateTime.parse(value, formatter);
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, LocalDateTime value) {
		return value.format(formatter);
	}

}
