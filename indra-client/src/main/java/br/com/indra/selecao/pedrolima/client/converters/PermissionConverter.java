package br.com.indra.selecao.pedrolima.client.converters;

import java.util.LinkedHashMap;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.springframework.stereotype.Component;

@Component(value = "permissionConverter")
@FacesConverter(value = "permissionConverter")
public class PermissionConverter implements Converter<LinkedHashMap<String, Object>> {

	@Override
	@SuppressWarnings("unchecked")
	public LinkedHashMap<String, Object> getAsObject(FacesContext context, UIComponent component, String value) {
		return (LinkedHashMap<String, Object>) GenericSerializer.fromString(value);
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, LinkedHashMap<String, Object> value) {
		return GenericSerializer.toString(value);
	}

}
