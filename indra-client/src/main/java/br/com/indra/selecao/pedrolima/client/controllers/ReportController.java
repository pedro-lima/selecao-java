package br.com.indra.selecao.pedrolima.client.controllers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import br.com.indra.selecao.pedrolima.client.services.report.ReportService;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Scope(value = "session")
@Component(value = "reportController")
public class ReportController {

	@Autowired
	private ReportService reportService;

	private Integer type;

	public void generate() {
		try {
			File file = generateFileReport();
			downloadFileReport(file);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private File generateFileReport() {
		File file;
		switch (type) {
		case 1:
			file = reportService.generateAvgPricesCommercialOperationByCityReport();
			break;
		case 2:
			file = reportService.generateAvgPricesCommercialOperationByFlagReport();
			break;
		case 3:
			file = reportService.generateAvgPricesPriceHistoryByCityReport();
			break;
		case 4:
			file = reportService.generateCommercialOperationByDateReport();
			break;
		case 5:
			file = reportService.generateCommercialOperationByFlagReport();
			break;
		case 6:
			file = reportService.generatecommercialOperationByRegionReport();
			break;
		default:
			file = null;
		}

		return file;
	}

	private void downloadFileReport(File file) throws IOException, FileNotFoundException {
		if (file == null) {
			return;
		}

		FacesContext facesContext = FacesContext.getCurrentInstance();

		HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();

		response.reset();
		response.setHeader("Content-Type", "application/pdf");
		response.setHeader("Content-Disposition", "attachment;filename=" + UUID.randomUUID().toString() + ".pdf");

		OutputStream responseOutputStream = response.getOutputStream();

		InputStream fileInputStream = new FileInputStream(file);

		byte[] bytesBuffer = new byte[2048];
		int bytesRead;
		while ((bytesRead = fileInputStream.read(bytesBuffer)) > 0) {
			responseOutputStream.write(bytesBuffer, 0, bytesRead);
		}

		responseOutputStream.flush();

		fileInputStream.close();
		responseOutputStream.close();

		facesContext.responseComplete();
	}

}
