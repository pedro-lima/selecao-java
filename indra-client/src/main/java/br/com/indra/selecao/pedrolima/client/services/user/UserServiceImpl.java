package br.com.indra.selecao.pedrolima.client.services.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.indra.selecao.pedrolima.client.config.ConfigProperty;
import br.com.indra.selecao.pedrolima.client.models.User;
import br.com.indra.selecao.pedrolima.client.services.GenericServiceImp;

@Service("userService")
public class UserServiceImpl extends GenericServiceImp<User> implements UserService {

	@Autowired
	private ConfigProperty configProperty;

	@Override
	public String getUrlService() {
		return configProperty.getUrlApi() + "user";
	}

}
