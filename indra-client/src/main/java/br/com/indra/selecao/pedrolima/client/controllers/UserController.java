package br.com.indra.selecao.pedrolima.client.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import br.com.indra.selecao.pedrolima.client.models.Permission;
import br.com.indra.selecao.pedrolima.client.models.User;
import br.com.indra.selecao.pedrolima.client.services.permission.PermissionService;
import br.com.indra.selecao.pedrolima.client.services.user.UserService;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Scope(value = "session")
@Component(value = "userController")
public class UserController {

	@Autowired
	private UserService userService;

	@Autowired
	private PermissionService permissionService;

	private User user;

	private List<User> users;

	private List<Permission> permissions;

	public void reset() {
		this.user = new User();

		this.permissions = new ArrayList<>();
		this.permissions.addAll(this.permissionService.list());
	}

	public void resetList() {
		this.users = new ArrayList<User>();
		this.users.addAll(this.userService.list());
	}

	public void delete(long id) {
		this.userService.delete(id);
		this.resetList();
	}

	public void save() {
		this.userService.save(this.user);
		this.reset();
	}

}
