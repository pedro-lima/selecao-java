package br.com.indra.selecao.pedrolima.client.services;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

public abstract class GenericServiceImp<T> implements GenericService<T> {

	@Override
	public void save(T obj) {
		Client client = ClientBuilder.newClient();
		WebTarget webTarget = client.target(getUrlService());

		Invocation.Builder invocationBuilder = webTarget.request("application/json;charset=UTF-8");
		invocationBuilder.post(Entity.entity(obj, "application/json;charset=UTF-8"));
	}

	@Override
	@SuppressWarnings("unchecked")
	public T find(long id) {
		Client client = ClientBuilder.newClient();
		WebTarget webTarget = client.target(getUrlService());

		Invocation.Builder invocationBuilder = webTarget.request("application/json;charset=UTF-8");

		Response response = invocationBuilder.get();

		return (T) response.readEntity(Object.class);
	}

	@Override
	public void delete(long id) {
		Client client = ClientBuilder.newClient();
		WebTarget webTarget = client.target(getUrlService()).path(String.valueOf(id));

		Invocation.Builder invocationBuilder = webTarget.request("application/json;charset=UTF-8");

		invocationBuilder.delete();
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<T> list() {
		Client client = ClientBuilder.newClient();
		WebTarget webTarget = client.target(getUrlService());

		Invocation.Builder invocationBuilder = webTarget.request("application/json;charset=UTF-8");

		Response response = invocationBuilder.get();

		return response.readEntity(ArrayList.class);
	}

	public abstract String getUrlService();

}
