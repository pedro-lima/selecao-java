package br.com.indra.selecao.pedrolima.client.models;

import java.io.Serializable;
import java.time.LocalDateTime;

import lombok.Data;

@Data
public class PriceHistory implements Serializable {

	private static final long serialVersionUID = 1311231650346816729L;

	private Long id;

	private LocalDateTime registrationDate;

	private ProductType productType;

	private double price;

	private StateType state;

	private String city;

}
