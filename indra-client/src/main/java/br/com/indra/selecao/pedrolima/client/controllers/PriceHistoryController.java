package br.com.indra.selecao.pedrolima.client.controllers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import br.com.indra.selecao.pedrolima.client.models.PriceHistory;
import br.com.indra.selecao.pedrolima.client.models.ProductType;
import br.com.indra.selecao.pedrolima.client.models.StateType;
import br.com.indra.selecao.pedrolima.client.services.priceHistory.PriceHistoryService;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Scope(value = "session")
@Component(value = "priceHistoryController")
public class PriceHistoryController {

	@Autowired
	private PriceHistoryService priceHistoryService;

	private PriceHistory price;

	private List<PriceHistory> prices;

	private List<StateType> stateTypes;

	private List<ProductType> productTypes;

	@PostConstruct
	private void init() {
		this.stateTypes = Arrays.asList(StateType.values());
		this.productTypes = Arrays.asList(ProductType.values());
	}

	public void reset() {
		this.price = new PriceHistory();
	}

	public void resetList() {
		this.prices = new ArrayList<>();
		this.prices.addAll(this.priceHistoryService.list());
	}

	public void delete(long id) {
		this.priceHistoryService.delete(id);
		this.resetList();
	}

	public void save() {
		this.priceHistoryService.save(price);
		this.reset();
	}

}
