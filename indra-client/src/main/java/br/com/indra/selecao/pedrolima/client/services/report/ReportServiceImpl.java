package br.com.indra.selecao.pedrolima.client.services.report;

import java.io.File;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.indra.selecao.pedrolima.client.config.ConfigProperty;

@Service("reportService")
public class ReportServiceImpl implements ReportService {

	@Autowired
	private ConfigProperty configProperty;

	public String getUrlService() {
		return configProperty.getUrlApi() + "report/";
	}

	@Override
	public File generateAvgPricesCommercialOperationByCityReport() {
		return getReport("avgPricesCommercialOperationByCity");
	}

	@Override
	public File generateAvgPricesCommercialOperationByFlagReport() {
		return getReport("avgPricesCommercialOperationByFlag");
	}

	@Override
	public File generateAvgPricesPriceHistoryByCityReport() {
		return getReport("avgPricesPriceHistoryByCity");
	}

	@Override
	public File generateCommercialOperationByDateReport() {
		return getReport("commercialOperationByDate");
	}

	@Override
	public File generateCommercialOperationByFlagReport() {
		return getReport("commercialOperationByFlag");
	}

	@Override
	public File generatecommercialOperationByRegionReport() {
		return getReport("commercialOperationByRegion");
	}

	public File getReport(String url) {
		try {
			Client client = ClientBuilder.newClient();
			WebTarget webTarget = client.target(getUrlService() + url);

			Invocation.Builder invocationBuilder = webTarget.request("application/pdf");

			Response response = invocationBuilder.get();
			if (response.getStatus() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
			}

			return response.readEntity(File.class);
		} catch (Exception e) {
			return null;
		}
	}

}
