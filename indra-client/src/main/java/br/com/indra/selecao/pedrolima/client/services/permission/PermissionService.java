package br.com.indra.selecao.pedrolima.client.services.permission;

import br.com.indra.selecao.pedrolima.client.models.Permission;
import br.com.indra.selecao.pedrolima.client.services.GenericService;

public interface PermissionService extends GenericService<Permission> {

}
