package br.com.indra.selecao.pedrolima.client.converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.springframework.stereotype.Component;

import br.com.indra.selecao.pedrolima.client.models.StateType;

@Component(value = "stateTypeConverter")
@FacesConverter(value = "stateTypeConverter")
public class StateTypeConverter implements Converter<StateType> {

	@Override
	public StateType getAsObject(FacesContext context, UIComponent component, String value) {
		return StateType.forValue(value);
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, StateType value) {
		return value.eventName();
	}

}
