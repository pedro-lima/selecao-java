package br.com.indra.selecao.pedrolima.client.services.report;

import java.io.File;

public interface ReportService {

	public File generateAvgPricesCommercialOperationByCityReport();

	public File generateAvgPricesCommercialOperationByFlagReport();

	public File generateAvgPricesPriceHistoryByCityReport();

	public File generateCommercialOperationByDateReport();

	public File generateCommercialOperationByFlagReport();

	public File generatecommercialOperationByRegionReport();

}
