package br.com.indra.selecao.pedrolima.client.services.permission;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.indra.selecao.pedrolima.client.config.ConfigProperty;
import br.com.indra.selecao.pedrolima.client.models.Permission;
import br.com.indra.selecao.pedrolima.client.services.GenericServiceImp;

@Service("permissionService")
public class PermissionServiceImpl extends GenericServiceImp<Permission> implements PermissionService {

	@Autowired
	private ConfigProperty configProperty;

	@Override
	public String getUrlService() {
		return configProperty.getUrlApi() + "permission";
	}

}
