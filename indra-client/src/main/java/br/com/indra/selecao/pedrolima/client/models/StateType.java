package br.com.indra.selecao.pedrolima.client.models;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum StateType {
	AC("AC"), AL("AL"), AP("AP"), AM("AM"), BA("BA"), CE("CE"), DF("DF"), ES("ES"), GO("GO"), MA("MA"), MT("MT"),
	MS("MS"), MG("MG"), PA("PA"), PB("PB"), PR("PR"), PE("PE"), PI("PI"), RJ("RJ"), RN("RN"), RS("RS"), RO("RO"),
	RR("RR"), SC("SC"), SP("SP"), SE("SE"), TO("TO");

	private static Map<String, StateType> nameMap = new HashMap<>(StateType.values().length);

	private final String eventName;

	@JsonCreator
	public static StateType forValue(String value) {
		return nameMap.get(value);
	}

	@JsonValue
	public String eventName() {
		return getEventName();
	}

	static {
		for (StateType eventType : StateType.values()) {
			nameMap.put(eventType.getEventName(), eventType);
		}
	}

}
