package br.com.indra.selecao.pedrolima.client.models;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum ProductType {
	DIESEL("D", "Diesel"), GAS("G", "Gas"), ETANOL("E", "Etanol"), NGV("N", "Natural gas vehicular");

	private static Map<String, ProductType> nameMap = new HashMap<>(ProductType.values().length);

	private final String eventName;

	private final String label;

	@JsonCreator
	public static ProductType forValue(String value) {
		return nameMap.get(value);
	}

	@JsonValue
	public String eventName() {
		return getEventName();
	}

	static {
		for (ProductType eventType : ProductType.values()) {
			nameMap.put(eventType.getEventName(), eventType);
		}
	}
}
