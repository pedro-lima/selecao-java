package br.com.indra.selecao.pedrolima.client.services.priceHistory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.indra.selecao.pedrolima.client.config.ConfigProperty;
import br.com.indra.selecao.pedrolima.client.models.PriceHistory;
import br.com.indra.selecao.pedrolima.client.services.GenericServiceImp;

@Service("priceHistoryService")
public class PriceHistoryServiceImpl extends GenericServiceImp<PriceHistory> implements PriceHistoryService {

	@Autowired
	private ConfigProperty configProperty;

	@Override
	public String getUrlService() {
		return configProperty.getUrlApi() + "price-history";
	}

}
