package br.com.indra.selecao.pedrolima.api.services.user;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.indra.selecao.pedrolima.api.exceptions.EntityNotFoundException;
import br.com.indra.selecao.pedrolima.api.models.User;
import br.com.indra.selecao.pedrolima.api.repositories.user.UserRepository;
import br.com.indra.selecao.pedrolima.api.services.GenericServiceImp;

@Service("userService")
public class UserServiceImpl extends GenericServiceImp<User> implements UserService {

	/**
	 * The repository of the User
	 */
	@Autowired
	private UserRepository repository;

	/**
	 * Get the repository of the User
	 */
	protected UserRepository getRepository() {
		return this.repository;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	//TODO: 
	public User save(User user) {
		checkLoginUser(user);

		// user.setPassword(new
		// BCryptPasswordEncoder().encode(user.getPassword().toLowerCase()));

		return getRepository().save(user);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	//TODO: 
	public User edit(long codigo, User user) {
		checkLoginUser(user);

		User userDB = find(codigo).orElseThrow(EntityNotFoundException::new);

		/*
		 * if (!StringUtils.isEmpty(user.getPassword())) { String password =
		 * user.getPassword().toLowerCase(); user.setPassword(new
		 * BCryptPasswordEncoder().encode(password)); }
		 */

		BeanUtils.copyProperties(user, userDB, "id");

		return getRepository().save(userDB);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public void checkLoginUser(User user) {
		getRepository().checkLoginUser(user);
	}

}
