package br.com.indra.selecao.pedrolima.api.repositories.commercialOperation;

import br.com.indra.selecao.pedrolima.api.models.CommercialOperation;
import br.com.indra.selecao.pedrolima.api.repositories.GenericRepositoriy;

public interface CommercialOperationRepository
		extends GenericRepositoriy<CommercialOperation>, CommercialOperationRepositoryQuery {

}
