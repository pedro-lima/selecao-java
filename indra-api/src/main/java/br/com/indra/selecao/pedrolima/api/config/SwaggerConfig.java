package br.com.indra.selecao.pedrolima.api.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Tag;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

	/**
	 * <p>
	 * Method responsible for configuring the Swagger. This service can be acessed
	 * by the fowlling links:
	 * </p>
	 * <ul>
	 * <li>http://localhost:8080/swagger-ui.html</li>
	 * <li>http://localhost:8080/v2/api-docs</li>
	 * </ul>
	 * 
	 * @return The bean responsible for containing the settings needed to run the
	 *         Swagger
	 */
	@Bean
	public Docket api() {
		ApiInfo apiInfo = new ApiInfoBuilder().title("Indra Exam API").description("Indra Exam API").version("1.0")
				.build();

		return new Docket(DocumentationType.SWAGGER_2).select()
				.apis(RequestHandlerSelectors.basePackage("br.com.indra.selecao.pedrolima")).paths(PathSelectors.any())
				.build().apiInfo(apiInfo).tags(new Tag("user", "Operations pertaining to user."),
						new Tag("price-history", "Operations pertaining to price history."),
						new Tag("import", "Operations pertaining to import csv file."),
						new Tag("report", "Operations pertaining to generate report file."),
						new Tag("permission", "Operations pertaining to permission."));
	}
}
