package br.com.indra.selecao.pedrolima.api.batch.dto;

import lombok.Data;

@Data
public class CommercialOperationDTO {

	private String region;

	private String state;

	private String city;

	private String installation;

	private String code;

	private String product;

	private String registrationDate;

	private String purchasePrice;

	private String saleValue;

	private String unitMeasurement;

	private String flag;

}
