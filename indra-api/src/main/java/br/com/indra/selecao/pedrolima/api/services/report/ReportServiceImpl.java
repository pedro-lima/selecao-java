package br.com.indra.selecao.pedrolima.api.services.report;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.indra.selecao.pedrolima.api.exceptions.FailGenerateReportException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;

@Service("reportService")
public class ReportServiceImpl implements ReportService {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public byte[] generateAvgPricesCommercialOperationByCityReport() {
		return generateGenericReport("report/avgPricesCommercialOperationByCity.jrxml");
	}

	@Override
	public byte[] generateAvgPricesCommercialOperationByFlagReport() {
		return generateGenericReport("report/avgPricesCommercialOperationByFlag.jrxml");
	}

	@Override
	public byte[] generateAvgPricesPriceHistoryByCityReport() {
		return generateGenericReport("report/avgPricesPriceHistoryByCity.jrxml");
	}

	@Override
	public byte[] generateCommercialOperationByDateReport() {
		return generateGenericReport("report/commercialOperationByDate.jrxml");
	}

	@Override
	public byte[] generateCommercialOperationByFlagReport() {
		return generateGenericReport("report/commercialOperationByFlag.jrxml");
	}

	@Override
	public byte[] generatecommercialOperationByRegionReport() {
		return generateGenericReport("report/commercialOperationByRegion.jrxml");
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	private byte[] generateGenericReport(String file) {
		try {
			ClassLoader classLoader = ClassLoader.getSystemClassLoader();
			String fileName = classLoader.getResource(file).getFile();
			JasperReport jasperReport = JasperCompileManager.compileReport(fileName);

			Map<String, Object> params = new HashMap<String, Object>();

			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, getConnection());
			return JasperExportManager.exportReportToPdf(jasperPrint);
		} catch (Exception e) {
			throw new FailGenerateReportException(e);
		}
	}
	
	private Connection getConnection() throws SQLException {
		return this.jdbcTemplate.getDataSource().getConnection();
	}

}
