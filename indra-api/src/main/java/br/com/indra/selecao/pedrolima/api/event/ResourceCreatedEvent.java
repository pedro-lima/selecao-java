package br.com.indra.selecao.pedrolima.api.event;

import javax.servlet.http.HttpServletResponse;

import org.springframework.context.ApplicationEvent;

import lombok.Getter;

@Getter
public class ResourceCreatedEvent extends ApplicationEvent {

	private static final long serialVersionUID = 1L;

	private HttpServletResponse response;

	private long id;

	public ResourceCreatedEvent(Object source, HttpServletResponse response, long id) {
		super(source);
		this.response = response;
		this.id = id;
	}

}
