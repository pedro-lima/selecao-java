package br.com.indra.selecao.pedrolima.api.models;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "t_user")
@Data
@EqualsAndHashCode(callSuper = false, of = { "id" })
@NoArgsConstructor
@SequenceGenerator(name = "user_gen", sequenceName = "seq_user", allocationSize = 1)
public class User {

	/**
	 * The database generated user ID
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_gen")
	@Column(name = "id", insertable = false, updatable = false)
	@ApiModelProperty(notes = "The database generated user ID")
	private Long id;

	/**
	 * The name of the user
	 */
	@NotEmpty
	@Column(name = "name", nullable = false, length = ConstantsUtils.MAX_SIZE_TEXT)
	@Size(min = ConstantsUtils.MIN_SIZE_TEXT, max = ConstantsUtils.MAX_SIZE_TEXT)
	@ApiModelProperty(notes = "The name of the user")
	private String name;

	/**
	 * The email of the user
	 */
	@NotEmpty
	@Column(name = "email", nullable = false, length = ConstantsUtils.MAX_SIZE_TEXT, unique = true)
	@Size(min = ConstantsUtils.MIN_SIZE_TEXT, max = ConstantsUtils.MAX_SIZE_TEXT)
	@ApiModelProperty(notes = "The email of the user")
	private String email;

	/**
	 * The password of the user
	 */
	@NotEmpty
	@Column(name = "password", nullable = false, length = ConstantsUtils.MAX_SIZE_TEXT)
	@Size(min = ConstantsUtils.MIN_SIZE_TEXT, max = ConstantsUtils.MAX_SIZE_TEXT)
	@ApiModelProperty(notes = "The password of the user")
	private String password;

	/**
	 * The permissions of the user
	 */
	@Size(min = 1)
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "user_permission", joinColumns = @JoinColumn(name = "id_user"), inverseJoinColumns = @JoinColumn(name = "id_permission"))
	private Set<Permission> permissions;

}
