package br.com.indra.selecao.pedrolima.api.batch;

import java.net.MalformedURLException;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import br.com.indra.selecao.pedrolima.api.batch.dto.CommercialOperationDTO;
import br.com.indra.selecao.pedrolima.api.models.CommercialOperation;

@Component
public class ImportCsvJob {

	private static final Logger log = LoggerFactory.getLogger(ImportCsvJob.class);

	private final JobBuilderFactory jobBuilderFactory;

	private final StepBuilderFactory stepBuilderFactory;

	private final NamedParameterJdbcTemplate jdbcTemplate;

	private final DataSource dataSource;

	private final CustomJobExecutionListenerSupport listener;

	public ImportCsvJob(JobBuilderFactory jobBuilderFactory, StepBuilderFactory stepBuilderFactory,
			NamedParameterJdbcTemplate jdbcTemplate, DataSource dataSource,
			CustomJobExecutionListenerSupport listener) {
		this.jobBuilderFactory = jobBuilderFactory;
		this.stepBuilderFactory = stepBuilderFactory;
		this.jdbcTemplate = jdbcTemplate;
		this.dataSource = dataSource;
		this.listener = listener;
	}

	private FlatFileItemReader<CommercialOperationDTO> reader() throws MalformedURLException {
		Path filePath = Paths.get(FilePathContext.getCurrentFilePath());
		Resource resource = new UrlResource(filePath.toUri());

		FlatFileItemReader<CommercialOperationDTO> reader = new FlatFileItemReader<>();
		reader.setResource(resource);
		reader.setLinesToSkip(1);
		reader.setLineMapper(new DefaultLineMapper<CommercialOperationDTO>() {
			{
				setLineTokenizer(new DelimitedLineTokenizer() {
					{
						setNames(new String[] { "region", "state", "city", "installation", "code", "product",
								"registrationDate", "purchasePrice", "saleValue", "unitMeasurement", "flag" });
						setDelimiter("  ");
					}
				});
				setFieldSetMapper(new BeanWrapperFieldSetMapper<CommercialOperationDTO>() {
					{
						setTargetType(CommercialOperationDTO.class);
					}
				});

			}

			@Override
			public CommercialOperationDTO mapLine(String line, int lineNumber) throws Exception {
				CommercialOperationDTO dto = null;

				try {
					dto = super.mapLine(line, lineNumber);
				} catch (Exception e) {
					//the cvs file used in this exam is broken in several lines
					log.error("Unable to parse line number <<{}>> with line <<{}>>.", lineNumber, line);
				}

				return dto;
			}
		});
		return reader;
	}

	private CustomItemProcessor processor() {
		return new CustomItemProcessor();
	}

	private JdbcBatchItemWriter<CommercialOperation> writer() {
		String sql = "INSERT INTO t_commercial_operation (id, region, state, city, installation, code, product, registration_date,";
		sql += " purchase_price, sale_value, unit_measurement, flag) VALUES (seq_commercial_operation.NEXTVAL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )";

		JdbcBatchItemWriter<CommercialOperation> writer = new JdbcBatchItemWriter<CommercialOperation>();
		writer.setItemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<>());
		writer.setSql(sql);
		writer.setDataSource(this.dataSource);
		writer.setJdbcTemplate(this.jdbcTemplate);
		writer.setItemPreparedStatementSetter(new CustomItemPreparedStatementSetter());

		return writer;
	}

	public Job getJob() throws Exception {
		return jobBuilderFactory.get("importCommercialTransactionJob").incrementer(new RunIdIncrementer())
				.listener(listener).flow(step1()).end().build();

	}

	private Step step1() throws MalformedURLException {
		return stepBuilderFactory.get("step1").<CommercialOperationDTO, CommercialOperation>chunk(10).reader(reader())
				.processor(processor()).writer(writer()).build();
	}

}
