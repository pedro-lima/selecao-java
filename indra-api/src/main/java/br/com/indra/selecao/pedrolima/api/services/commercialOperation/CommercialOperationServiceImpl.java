package br.com.indra.selecao.pedrolima.api.services.commercialOperation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.indra.selecao.pedrolima.api.models.CommercialOperation;
import br.com.indra.selecao.pedrolima.api.repositories.commercialOperation.CommercialOperationRepository;
import br.com.indra.selecao.pedrolima.api.services.GenericServiceImp;

@Service("commercialOperationService")
public class CommercialOperationServiceImpl extends GenericServiceImp<CommercialOperation>
		implements CommercialOperationService {

	/**
	 * The repository of the CommercialOperation
	 */
	@Autowired
	private CommercialOperationRepository repository;

	/**
	 * Get the repository of the CommercialOperation
	 */
	protected CommercialOperationRepository getRepository() {
		return this.repository;
	}

}
