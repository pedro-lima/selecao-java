package br.com.indra.selecao.pedrolima.api.models;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "t_price_history")
@Data
@EqualsAndHashCode(callSuper = false, of = { "id" })
@NoArgsConstructor
@SequenceGenerator(name = "price_history_gen", sequenceName = "seq_price_history", allocationSize = 1)
public class PriceHistory {

	/**
	 * The database generated price history ID
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "price_history_gen")
	@Column(name = "id", insertable = false, updatable = false)
	@ApiModelProperty(notes = "The database generated price history ID")
	private Long id;

	/**
	 * The registration date of the price history
	 */
	@JsonIgnore
	@Column(name = "registration_date", nullable = false)
	@ApiModelProperty(notes = "The registration date of the price history")
	private LocalDateTime registrationDate;

	/**
	 * The product type of the price history
	 */
	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "product_type", nullable = false)
	@ApiModelProperty(notes = "The product type of the price history")
	private ProductType productType;

	/**
	 * The price of the price history
	 */
	@Min(0)
	@Column(name = "price")
	@ApiModelProperty(notes = "The price of the price history")
	private double price;

	/**
	 * The state of the price history
	 */
	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "state", nullable = false)
	@ApiModelProperty(notes = "The state of the price history")
	private StateType state;

	/**
	 * The city of the price history
	 */
	@NotEmpty
	@Column(name = "city", length = ConstantsUtils.MAX_SIZE_TEXT, nullable = false)
	@Size(min = ConstantsUtils.MIN_SIZE_TEXT, max = ConstantsUtils.MAX_SIZE_TEXT)
	@ApiModelProperty(notes = "The city of the price history")
	private String city;

	@PrePersist
	public void prePersist() {
		setRegistrationDate(LocalDateTime.now());
	}

	@PreUpdate
	public void preUpdate() {
		setRegistrationDate(LocalDateTime.now());
	}

}
