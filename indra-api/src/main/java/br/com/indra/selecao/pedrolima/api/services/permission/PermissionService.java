package br.com.indra.selecao.pedrolima.api.services.permission;

import br.com.indra.selecao.pedrolima.api.models.Permission;
import br.com.indra.selecao.pedrolima.api.services.GenericService;

public interface PermissionService extends GenericService<Permission> {

}
