package br.com.indra.selecao.pedrolima.api.exceptions;

public class GenericException extends RuntimeException {

	private static final long serialVersionUID = -1814022663256454246L;

	public GenericException() {

	}

	public GenericException(String message) {
		super(message);
	}

	public GenericException(Throwable cause) {
		super(cause);
	}

	public GenericException(String message, Throwable cause) {
		super(message, cause);
	}

}