package br.com.indra.selecao.pedrolima.api.repositories.priceHistory;

import br.com.indra.selecao.pedrolima.api.models.PriceHistory;
import br.com.indra.selecao.pedrolima.api.repositories.GenericRepositoriy;

public interface PriceHistoryRepository extends GenericRepositoriy<PriceHistory>, PriceHistoryRepositoryQuery {

}
