package br.com.indra.selecao.pedrolima.api.batch;

public class FilePathContext {

	final public static String DEFAULT_FILE = "";

	private static ThreadLocal<String> currentFilePath = new ThreadLocal<String>() {
		@Override
		protected String initialValue() {
			return DEFAULT_FILE;
		}
	};

	public static void setCurrentFilePath(String filePath) {
		currentFilePath.set(filePath);
	}

	public static String getCurrentFilePath() {
		return currentFilePath.get();
	}

	public static void clear() {
		currentFilePath.remove();
	}
}
