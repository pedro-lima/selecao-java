package br.com.indra.selecao.pedrolima.api.resources;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.indra.selecao.pedrolima.api.models.Permission;
import br.com.indra.selecao.pedrolima.api.services.permission.PermissionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/api/permission")
@Api(tags = { "permission" })
@CrossOrigin
public class PermissionResource {

	@Autowired
	private PermissionService permissionService;

	@GetMapping
	@ApiOperation(value = "View a list of permission", response = Iterable.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Resource successfully retrieved"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	public Iterable<Permission> list() {
		return permissionService.list();
	}

}
