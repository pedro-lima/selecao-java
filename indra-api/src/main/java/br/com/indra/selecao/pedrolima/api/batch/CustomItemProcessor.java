package br.com.indra.selecao.pedrolima.api.batch;

import java.text.NumberFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;

import br.com.indra.selecao.pedrolima.api.batch.dto.CommercialOperationDTO;
import br.com.indra.selecao.pedrolima.api.models.CommercialOperation;

public class CustomItemProcessor implements ItemProcessor<CommercialOperationDTO, CommercialOperation> {

	private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("dd/MM/yyyy");

	private static NumberFormat format = NumberFormat.getInstance(Locale.FRANCE);

	private static final Logger log = LoggerFactory.getLogger(CustomItemProcessor.class);

	@Override
	public CommercialOperation process(CommercialOperationDTO dto) throws Exception {

		CommercialOperation operation = new CommercialOperation();
		operation.setInstallation(dto.getInstallation());
		operation.setFlag(dto.getFlag());

		if (StringUtils.isNotEmpty(dto.getRegistrationDate())) {
			LocalDate date;
			try {
				date = LocalDate.parse(dto.getRegistrationDate(), DATE_FORMATTER);
			} catch (Exception e) {
				date = null;
			}
			operation.setRegistrationDate(date);
		}

		CommercialOperation.Item item = new CommercialOperation.Item();

		if (StringUtils.isNotEmpty(dto.getPurchasePrice())) {
			Double value;

			try {
				value = format.parse(dto.getPurchasePrice()).doubleValue();
			} catch (Exception e) {
				value = null;
			}
			item.setPurchasePrice(value);
		}

		if (StringUtils.isNotEmpty(dto.getSaleValue())) {
			Double value;

			try {
				value = format.parse(dto.getSaleValue()).doubleValue();
			} catch (Exception e) {
				value = null;
			}
			item.setSaleValue(value);

		}

		operation.setItem(item);

		CommercialOperation.Location location = new CommercialOperation.Location();
		location.setCity(dto.getCity());
		location.setRegion(dto.getRegion());
		location.setState(dto.getState());
		operation.setLocation(location);

		CommercialOperation.Product product = new CommercialOperation.Product();
		product.setCode(NumberUtils.isCreatable(dto.getCode()) ? Long.parseLong(dto.getCode()) : null);
		product.setProduct(dto.getProduct());
		product.setUnitMeasurement(dto.getUnitMeasurement());
		operation.setProduct(product);

		log.info("Converting (" + dto + ") into (" + operation + ")");

		return operation;
	}

}
