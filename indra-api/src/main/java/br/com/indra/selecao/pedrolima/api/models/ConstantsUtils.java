package br.com.indra.selecao.pedrolima.api.models;

import java.io.File;

public class ConstantsUtils {

	public static final int MAX_SIZE_TEXT = 255;

	public static final int MIN_SIZE_TEXT = 3;
	
	public static final int MAX_SIZE_TEXT_LOCATION = 3;

	public static final int MIN_SIZE_TEXT_LOCATION = 1;


	private static final String UPLOADED_FOLDER = "/tmp/indra/csv/";

	public static String getPath() {
		File directory = new File(UPLOADED_FOLDER);
		if (!directory.exists()) {
			directory.mkdirs();
		}

		return UPLOADED_FOLDER;
	}

}
