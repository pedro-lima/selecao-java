package br.com.indra.selecao.pedrolima.api.services;

import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.indra.selecao.pedrolima.api.exceptions.EntityNotFoundException;

public abstract class GenericServiceImp<T> implements GenericService<T> {

	/**
	 * Saves a given entity. Use the returned instance for further operations as the
	 * save operation might have changed the entity instance completely.
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public T save(T obj) {
		return getRepository().save(obj);
	}

	/**
	 * Edits a given entity. Use the returned instance for further operations as the
	 * edit operation might have changed the entity instance completely.
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public T edit(T obj) {
		return getRepository().save(obj);
	}

	/**
	 * Edits a given entity. Use the returned instance for further operations as the
	 * edit operation might have changed the entity instance completely.
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public T edit(long id, T obj) {
		T t = find(id).orElseThrow(EntityNotFoundException::new);
		BeanUtils.copyProperties(obj, t, "id");

		return getRepository().save(t);
	}

	/**
	 * Retrieves an entity by its id.
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public Optional<T> find(long id) {
		return getRepository().findById(id);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void delete(long id) {
		T t = find(id).orElseThrow(EntityNotFoundException::new);
		delete(t);
	}

	/**
	 * Deletes a given entity.
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void delete(T obj) {
		getRepository().delete(obj);
	}

	/**
	 * Returns all instances of the type.
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public Iterable<T> list() {
		return getRepository().findAll();
	}

	/**
	 * Get the repository used by the child class
	 */
	protected abstract CrudRepository<T, Long> getRepository();

}
