package br.com.indra.selecao.pedrolima.api.services.priceHistory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.indra.selecao.pedrolima.api.models.PriceHistory;
import br.com.indra.selecao.pedrolima.api.repositories.priceHistory.PriceHistoryRepository;
import br.com.indra.selecao.pedrolima.api.services.GenericServiceImp;

@Service("priceHistoryService")
public class PriceHistoryServiceImpl extends GenericServiceImp<PriceHistory> implements PriceHistoryService {

	/**
	 * The repository of the PriceHistory
	 */
	@Autowired
	private PriceHistoryRepository repository;

	/**
	 * Get the repository of the PriceHistory
	 */
	protected PriceHistoryRepository getRepository() {
		return this.repository;
	}

}
