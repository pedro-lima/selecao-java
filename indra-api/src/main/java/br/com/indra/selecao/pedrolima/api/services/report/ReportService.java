package br.com.indra.selecao.pedrolima.api.services.report;

public interface ReportService {

	public byte[] generateAvgPricesCommercialOperationByCityReport();

	public byte[] generateAvgPricesCommercialOperationByFlagReport();

	public byte[] generateAvgPricesPriceHistoryByCityReport();

	public byte[] generateCommercialOperationByDateReport();

	public byte[] generateCommercialOperationByFlagReport();

	public byte[] generatecommercialOperationByRegionReport();

}
