package br.com.indra.selecao.pedrolima.api.exceptions;

public class EntityNotFoundException extends GenericException {

	private static final long serialVersionUID = -7847011151089273396L;

	public EntityNotFoundException() {

	}

	public EntityNotFoundException(String message) {
		super(message);
	}

	public EntityNotFoundException(Throwable cause) {
		super(cause);
	}

	public EntityNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}

}