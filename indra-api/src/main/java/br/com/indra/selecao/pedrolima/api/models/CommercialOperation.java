package br.com.indra.selecao.pedrolima.api.models;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "t_commercial_operation")
@Data
@EqualsAndHashCode(callSuper = false, of = { "id" })
@NoArgsConstructor
@SequenceGenerator(name = "commercial_operation_gen", sequenceName = "seq_commercial_operation", allocationSize = 1)
public class CommercialOperation {

	/**
	 * The database generated commercial operation ID
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "commercial_operation_gen")
	@Column(name = "id", insertable = false, updatable = false)
	@ApiModelProperty(notes = "The database generated commercial operation ID")
	private Long id;

	/**
	 * The installation of the commercial operation
	 */
	@Column(name = "installation", length = ConstantsUtils.MAX_SIZE_TEXT)
	@Size(min = ConstantsUtils.MIN_SIZE_TEXT, max = ConstantsUtils.MAX_SIZE_TEXT)
	@ApiModelProperty(notes = "The installation of the commercial operation")
	private String installation;

	/**
	 * The registration date of the commercial operation
	 */
	@Column(name = "registration_date")
	@ApiModelProperty(notes = "The registration date of the commercial operation")
	private LocalDate registrationDate;

	/**
	 * The flag of the commercial operation
	 */
	@Column(name = "flag", length = ConstantsUtils.MAX_SIZE_TEXT)
	@Size(min = ConstantsUtils.MIN_SIZE_TEXT, max = ConstantsUtils.MAX_SIZE_TEXT)
	@ApiModelProperty(notes = "The flag of the commercial operation")
	private String flag;

	/**
	 * The item of the commercial operation
	 */
	@Valid
	@NotNull
	@Embedded
	@ApiModelProperty(notes = "The item of the commercial operation")
	private Item item;

	/**
	 * The location of the commercial operation
	 */
	@Valid
	@NotNull
	@Embedded
	@ApiModelProperty(notes = "The location of the commercial operation")
	private Location location;

	/**
	 * The product of the commercial operation
	 */
	@Valid
	@NotNull
	@Embedded
	@ApiModelProperty(notes = "The product of the commercial operation")
	private Product product;

	@ToString
	@Getter
	@Setter
	@Embeddable
	public static class Item {

		/**
		 * The purchase price of the item
		 */
		@Column(name = "purchase_price")
		@ApiModelProperty(notes = "The purchase price of the item")
		private Double purchasePrice;

		/**
		 * The sale value of the item
		 */
		@Column(name = "sale_value")
		@ApiModelProperty(notes = "The sale value of the item")
		private Double saleValue;

	}

	@ToString
	@Getter
	@Setter
	@Embeddable
	public static class Location {

		/**
		 * The region of the location
		 */
		@Column(name = "region", length = ConstantsUtils.MAX_SIZE_TEXT_LOCATION)
		@Size(min = ConstantsUtils.MIN_SIZE_TEXT_LOCATION, max = ConstantsUtils.MAX_SIZE_TEXT_LOCATION)
		@ApiModelProperty(notes = "The region of the location")
		private String region;

		/**
		 * The state of the location
		 */
		@Column(name = "state", length = ConstantsUtils.MAX_SIZE_TEXT_LOCATION)
		@Size(min = ConstantsUtils.MIN_SIZE_TEXT_LOCATION, max = ConstantsUtils.MAX_SIZE_TEXT_LOCATION)
		@ApiModelProperty(notes = "The state of the location")
		private String state;

		/**
		 * The city of the location
		 */
		@Column(name = "city", length = ConstantsUtils.MAX_SIZE_TEXT)
		@Size(min = ConstantsUtils.MIN_SIZE_TEXT, max = ConstantsUtils.MAX_SIZE_TEXT)
		@ApiModelProperty(notes = "The city of the location")
		private String city;

	}

	@ToString
	@Getter
	@Setter
	@Embeddable
	public static class Product {

		/**
		 * The code of the product
		 */
		@Column(name = "code")
		@ApiModelProperty(notes = "The code of the product")
		private Long code;

		/**
		 * The name of the product
		 */
		@Column(name = "product")
		@ApiModelProperty(notes = "The name of the product")
		private String product;

		/**
		 * The unit measurement of the product
		 */
		@Column(name = "unit_measurement")
		@ApiModelProperty(notes = "The unit measurement of the product")
		private String unitMeasurement;

	}

}
