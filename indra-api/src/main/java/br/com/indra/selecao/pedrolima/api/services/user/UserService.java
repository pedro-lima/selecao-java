package br.com.indra.selecao.pedrolima.api.services.user;

import br.com.indra.selecao.pedrolima.api.models.User;
import br.com.indra.selecao.pedrolima.api.services.GenericService;

public interface UserService extends GenericService<User> {
	
	public void checkLoginUser(User user);

}
