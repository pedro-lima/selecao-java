package br.com.indra.selecao.pedrolima.api.services.permission;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.indra.selecao.pedrolima.api.models.Permission;
import br.com.indra.selecao.pedrolima.api.repositories.permission.PermissionRepository;
import br.com.indra.selecao.pedrolima.api.services.GenericServiceImp;

@Service("permissionService")
public class PermissionServiceImpl extends GenericServiceImp<Permission> implements PermissionService {

	/**
	 * The repository of the Permission
	 */
	@Autowired
	private PermissionRepository repository;

	/**
	 * Get the repository of the Permission
	 */
	protected PermissionRepository getRepository() {
		return this.repository;
	}

}
