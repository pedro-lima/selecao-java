package br.com.indra.selecao.pedrolima.api.batch;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.springframework.batch.item.database.ItemPreparedStatementSetter;

import br.com.indra.selecao.pedrolima.api.models.CommercialOperation;

public class CustomItemPreparedStatementSetter implements ItemPreparedStatementSetter<CommercialOperation> {

	@Override
	public void setValues(CommercialOperation operation, PreparedStatement preparedStatement) throws SQLException {
		int i = 1;
		preparedStatement.setObject(i++, operation.getLocation().getRegion());
		preparedStatement.setObject(i++, operation.getLocation().getState());
		preparedStatement.setObject(i++, operation.getLocation().getCity());
		preparedStatement.setObject(i++, operation.getInstallation());
		preparedStatement.setObject(i++, operation.getProduct().getCode());
		preparedStatement.setObject(i++, operation.getProduct().getProduct());
		preparedStatement.setObject(i++, operation.getRegistrationDate());
		preparedStatement.setObject(i++, operation.getItem().getPurchasePrice());
		preparedStatement.setObject(i++, operation.getItem().getSaleValue());
		preparedStatement.setObject(i++, operation.getProduct().getUnitMeasurement());
		preparedStatement.setObject(i++, operation.getFlag());
	}

}
