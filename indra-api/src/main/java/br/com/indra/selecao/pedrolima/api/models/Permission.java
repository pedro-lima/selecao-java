package br.com.indra.selecao.pedrolima.api.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "t_permission")
@Data
@EqualsAndHashCode(callSuper = false, of = { "id" })
@NoArgsConstructor
@SequenceGenerator(name = "permission_gen", sequenceName = "seq_permission", allocationSize = 1)
public class Permission {

	/**
	 * The database generated permission ID
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "permission_gen")
	@Column(name = "id", insertable = false, updatable = false)
	@ApiModelProperty(notes = "The database generated permission ID")
	private Long id;

	@NotEmpty
	@Column(name = "description", nullable = false, length = ConstantsUtils.MAX_SIZE_TEXT)
	@Size(min = ConstantsUtils.MIN_SIZE_TEXT, max = ConstantsUtils.MAX_SIZE_TEXT)
	@ApiModelProperty(notes = "The description of the permission")
	private String description;

	@NotEmpty
	@Column(name = "value", nullable = false, length = ConstantsUtils.MAX_SIZE_TEXT)
	@Size(min = ConstantsUtils.MIN_SIZE_TEXT, max = ConstantsUtils.MAX_SIZE_TEXT)
	@ApiModelProperty(notes = "The value of the permission")
	private String value;

}
