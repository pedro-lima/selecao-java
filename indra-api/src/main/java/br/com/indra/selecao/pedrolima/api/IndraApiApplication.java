package br.com.indra.selecao.pedrolima.api;

import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableBatchProcessing
public class IndraApiApplication {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(IndraApiApplication.class, args);
	}

}
