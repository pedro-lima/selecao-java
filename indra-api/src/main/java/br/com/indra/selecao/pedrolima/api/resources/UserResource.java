package br.com.indra.selecao.pedrolima.api.resources;

import java.util.Optional;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.indra.selecao.pedrolima.api.event.ResourceCreatedEvent;
import br.com.indra.selecao.pedrolima.api.models.User;
import br.com.indra.selecao.pedrolima.api.services.user.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/api/user")
@Api(tags = { "user" })
@CrossOrigin
public class UserResource {

	@Autowired
	private UserService userService;

	@Autowired
	private ApplicationEventPublisher publisher;

	@GetMapping
	@ApiOperation(value = "View a list of users", response = Iterable.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Resource successfully retrieved"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	public Iterable<User> list() {
		return userService.list();
	}

	@PostMapping
	@ApiOperation(value = "Save a user resource", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 201, message = "Resource saved successfully") })
	public ResponseEntity<User> save(@Valid @RequestBody User user, HttpServletResponse response) {
		User userSaved = userService.save(user);
		publisher.publishEvent(new ResourceCreatedEvent(this, response, userSaved.getId()));
		return ResponseEntity.status(HttpStatus.CREATED).body(userSaved);
	}

	@GetMapping("/{id}")
	@ApiOperation(value = "Retrieve a user resource that matches a given id", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Resource successfully retrieved"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	public ResponseEntity<User> find(@PathVariable Long id) {
		Optional<User> user = userService.find(id);
		return user.isPresent() ? ResponseEntity.ok(user.get()) : ResponseEntity.notFound().build();
	}

	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@ApiOperation(value = "Delete a user resource", response = ResponseEntity.class)
	public void delete(@PathVariable Long id) {
		userService.delete(id);
	}

	@PutMapping("/{id}")
	@ApiOperation(value = "Edit a user resource", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 201, message = "Resource edited successfully") })
	public ResponseEntity<User> edit(@PathVariable Long id, @Valid @RequestBody User user) {
		User userSaved = userService.edit(id, user);
		return ResponseEntity.ok(userSaved);
	}

}
