package br.com.indra.selecao.pedrolima.api.services.commercialOperation;

import br.com.indra.selecao.pedrolima.api.models.CommercialOperation;
import br.com.indra.selecao.pedrolima.api.services.GenericService;


public interface CommercialOperationService extends GenericService<CommercialOperation> {

}
