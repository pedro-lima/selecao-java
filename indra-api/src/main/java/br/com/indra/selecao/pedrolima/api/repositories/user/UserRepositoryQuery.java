package br.com.indra.selecao.pedrolima.api.repositories.user;

import br.com.indra.selecao.pedrolima.api.models.User;

public interface UserRepositoryQuery {
	
	public void checkLoginUser(User user);

}
