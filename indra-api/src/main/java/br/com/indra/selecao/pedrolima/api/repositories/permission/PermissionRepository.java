package br.com.indra.selecao.pedrolima.api.repositories.permission;

import br.com.indra.selecao.pedrolima.api.models.Permission;
import br.com.indra.selecao.pedrolima.api.repositories.GenericRepositoriy;

public interface PermissionRepository extends GenericRepositoriy<Permission>, PermissionRepositoryQuery {

}
