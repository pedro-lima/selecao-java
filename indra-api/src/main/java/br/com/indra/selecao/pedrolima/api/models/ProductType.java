package br.com.indra.selecao.pedrolima.api.models;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum ProductType {
	DIESEL("D"), GAS("G"), ETANOL("E"), NGV("N");

	private static Map<String, ProductType> nameMap = new HashMap<>(ProductType.values().length);

	private final String eventName;

	ProductType(String eventName) {
		this.eventName = eventName;
	}

	@JsonCreator
	public static ProductType forValue(String value) {
		return nameMap.get(value);
	}

	@JsonValue
	public String eventName() {
		return eventName;
	}

	static {
		for (ProductType eventType : ProductType.values()) {
			nameMap.put(eventType.eventName(), eventType);
		}
	}
}
