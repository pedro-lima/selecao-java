package br.com.indra.selecao.pedrolima.api.services;

import java.util.Optional;

public interface GenericService<T> {

	/**
	 * Saves a given entity. Use the returned instance for further operations as the
	 * save operation might have changed the entity instance completely.
	 */
	T save(T obj);

	/**
	 * Edits a given entity. Use the returned instance for further operations as the
	 * edit operation might have changed the entity instance completely.
	 */
	T edit(T obj);

	/**
	 * Edits a given entity. Use the returned instance for further operations as the
	 * edit operation might have changed the entity instance completely.
	 */
	T edit(long id, T obj);

	/**
	 * Retrieves an entity by its id.
	 */
	Optional<T> find(long id);

	/**
	 * Deletes a given entity.
	 */
	void delete(long id);

	/**
	 * Deletes a given entity.
	 */
	void delete(T obj);

	/**
	 * Returns all instances of the type.
	 */
	Iterable<T> list();

}
