package br.com.indra.selecao.pedrolima.api.repositories.user;

import br.com.indra.selecao.pedrolima.api.models.User;
import br.com.indra.selecao.pedrolima.api.repositories.GenericRepositoriy;

public interface UserRepository extends GenericRepositoriy<User>, UserRepositoryQuery {

}
