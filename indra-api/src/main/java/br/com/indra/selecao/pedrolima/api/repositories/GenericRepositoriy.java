package br.com.indra.selecao.pedrolima.api.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface GenericRepositoriy<T> extends JpaRepository<T, Long>, QuerydslPredicateExecutor<T> {

}
