package br.com.indra.selecao.pedrolima.api.resources;

import java.util.Optional;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.indra.selecao.pedrolima.api.event.ResourceCreatedEvent;
import br.com.indra.selecao.pedrolima.api.models.PriceHistory;
import br.com.indra.selecao.pedrolima.api.services.priceHistory.PriceHistoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/api/price-history")
@Api(tags = { "price-history" })
@CrossOrigin
public class PriceHistoryResource {

	@Autowired
	private PriceHistoryService priceHistoryService;

	@Autowired
	private ApplicationEventPublisher publisher;

	@GetMapping
	@ApiOperation(value = "View a list of price history", response = Iterable.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Resource successfully retrieved"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	public Iterable<PriceHistory> list() {
		return priceHistoryService.list();
	}

	@PostMapping
	@ApiOperation(value = "Save a price history resource", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 201, message = "Resource saved successfully") })
	public ResponseEntity<PriceHistory> save(@Valid @RequestBody PriceHistory priceHistory,
			HttpServletResponse response) {
		PriceHistory priceHistorySaved = priceHistoryService.save(priceHistory);
		publisher.publishEvent(new ResourceCreatedEvent(this, response, priceHistorySaved.getId()));
		return ResponseEntity.status(HttpStatus.CREATED).body(priceHistorySaved);
	}

	@GetMapping("/{id}")
	@ApiOperation(value = "Retrieve a price history resource that matches a given id", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Resource successfully retrieved"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	public ResponseEntity<PriceHistory> find(@PathVariable Long id) {
		Optional<PriceHistory> priceHistory = priceHistoryService.find(id);
		return priceHistory.isPresent() ? ResponseEntity.ok(priceHistory.get()) : ResponseEntity.notFound().build();
	}

	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@ApiOperation(value = "Delete a price history resource", response = ResponseEntity.class)
	public void delete(@PathVariable Long id) {
		priceHistoryService.delete(id);
	}

	@PutMapping("/{id}")
	@ApiOperation(value = "Edit a price history resource", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 201, message = "Resource edited successfully") })
	public ResponseEntity<PriceHistory> edit(@PathVariable Long id, @Valid @RequestBody PriceHistory priceHistory) {
		PriceHistory priceHistorySaved = priceHistoryService.edit(id, priceHistory);
		return ResponseEntity.ok(priceHistorySaved);
	}

}
