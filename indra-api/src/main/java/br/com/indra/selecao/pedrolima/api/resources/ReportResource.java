package br.com.indra.selecao.pedrolima.api.resources;

import java.util.UUID;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.indra.selecao.pedrolima.api.services.report.ReportService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/api/report")
@Api(tags = { "report" })
@CrossOrigin
public class ReportResource {

	@Autowired
	private ReportService reportService;

	@GetMapping("/avgPricesCommercialOperationByCity")
	@ApiOperation(value = "Generate a report", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Report generated successfully") })
	public HttpEntity<byte[]> generateAvgPricesCommercialOperationByCityReport(final HttpServletResponse response) {
		byte[] file = reportService.generateAvgPricesCommercialOperationByCityReport();
		return genericGenerateReport(file, response);
	}

	@GetMapping("/avgPricesCommercialOperationByFlag")
	@ApiOperation(value = "Generate a report", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Report generated successfully") })
	public HttpEntity<byte[]> generateAvgPricesCommercialOperationByFlagReport(final HttpServletResponse response) {
		byte[] file = reportService.generateAvgPricesCommercialOperationByFlagReport();
		return genericGenerateReport(file, response);
	}

	@GetMapping("/avgPricesPriceHistoryByCity")
	@ApiOperation(value = "Generate a report", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Report generated successfully") })
	public HttpEntity<byte[]> generateAvgPricesPriceHistoryByCityReport(final HttpServletResponse response) {
		byte[] file = reportService.generateAvgPricesPriceHistoryByCityReport();
		return genericGenerateReport(file, response);
	}

	@GetMapping("/commercialOperationByDate")
	@ApiOperation(value = "Generate a report", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Report generated successfully") })
	public HttpEntity<byte[]> generateCommercialOperationByDateReport(final HttpServletResponse response) {
		byte[] file = reportService.generateCommercialOperationByDateReport();
		return genericGenerateReport(file, response);
	}

	@GetMapping("/commercialOperationByFlag")
	@ApiOperation(value = "Generate a report", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Report generated successfully") })
	public HttpEntity<byte[]> generateCommercialOperationByFlagReport(final HttpServletResponse response) {
		byte[] file = reportService.generateCommercialOperationByFlagReport();
		return genericGenerateReport(file, response);
	}

	@GetMapping("/commercialOperationByRegion")
	@ApiOperation(value = "Generate a report", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Report generated successfully") })
	public HttpEntity<byte[]> generatecommercialOperationByRegionReport(final HttpServletResponse response) {
		byte[] file = reportService.generatecommercialOperationByRegionReport();
		return genericGenerateReport(file, response);
	}

	private HttpEntity<byte[]> genericGenerateReport(final byte[] arquivo, final HttpServletResponse response) {
		String uuid = UUID.randomUUID().toString();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_PDF);
		response.setContentLength(arquivo.length);
		response.setHeader("Content-Disposition", "attachment; filename=" + uuid + ".pdf");
		return new HttpEntity<byte[]>(arquivo, headers);
	}

}
