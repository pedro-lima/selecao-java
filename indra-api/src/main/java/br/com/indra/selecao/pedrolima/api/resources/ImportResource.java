package br.com.indra.selecao.pedrolima.api.resources;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import br.com.indra.selecao.pedrolima.api.batch.FilePathContext;
import br.com.indra.selecao.pedrolima.api.batch.ImportCsvJob;
import br.com.indra.selecao.pedrolima.api.models.ConstantsUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/api/import")
@Api(tags = { "import" })
@CrossOrigin
public class ImportResource {

	@Autowired
	JobLauncher jobLauncher;

	@Autowired
	ImportCsvJob job;

	@PostMapping
	@ApiOperation(value = "Import a csv file of commercial operations", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Resource saved successfully") })
	public ResponseEntity<?> uploadFile(@RequestParam("file") MultipartFile uploadfile) {
		if (uploadfile.isEmpty()) {
			return new ResponseEntity<String>("please select a file!", HttpStatus.OK);
		}

		try {
			FilePathContext.setCurrentFilePath(saveUploadedFiles(uploadfile));
			jobLauncher.run(job.getJob(), new JobParameters());
			return new ResponseEntity<String>("Successfully uploaded - " + uploadfile.getOriginalFilename(),
					new HttpHeaders(), HttpStatus.OK);
		} catch (IOException e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	private String saveUploadedFiles(MultipartFile file) throws IOException {
		byte[] bytes = file.getBytes();
		Path path = Paths.get(ConstantsUtils.getPath() + file.getOriginalFilename());
		Files.write(path, bytes);

		return path.toFile().getAbsolutePath();
	}

}
