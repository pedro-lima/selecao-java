package br.com.indra.selecao.pedrolima.api.exceptions;

public class EmailAlreadyRegisteredException extends GenericException {

	private static final long serialVersionUID = 3194180656198395656L;

	public EmailAlreadyRegisteredException() {

	}

	public EmailAlreadyRegisteredException(String message) {
		super(message);
	}

	public EmailAlreadyRegisteredException(Throwable cause) {
		super(cause);
	}

	public EmailAlreadyRegisteredException(String message, Throwable cause) {
		super(message, cause);
	}

}
