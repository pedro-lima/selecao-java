package br.com.indra.selecao.pedrolima.api.exceptions;

public class FailGenerateReportException extends GenericException {

	private static final long serialVersionUID = 5913189896367307931L;

	public FailGenerateReportException() {

	}

	public FailGenerateReportException(String message) {
		super(message);
	}

	public FailGenerateReportException(Throwable cause) {
		super(cause);
	}

	public FailGenerateReportException(String message, Throwable cause) {
		super(message, cause);
	}

}
