package br.com.indra.selecao.pedrolima.api.services.priceHistory;

import br.com.indra.selecao.pedrolima.api.models.PriceHistory;
import br.com.indra.selecao.pedrolima.api.services.GenericService;

public interface PriceHistoryService extends GenericService<PriceHistory> {
	
}
