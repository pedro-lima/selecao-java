package br.com.indra.selecao.pedrolima.api.repositories.user;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.util.StringUtils;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.jpa.JPQLQuery;
import com.querydsl.jpa.impl.JPAQuery;

import br.com.indra.selecao.pedrolima.api.exceptions.EmailAlreadyRegisteredException;
import br.com.indra.selecao.pedrolima.api.models.QUser;
import br.com.indra.selecao.pedrolima.api.models.User;

@SuppressWarnings("all")
public class UserRepositoryImpl implements UserRepositoryQuery {

	/***
	 * The entity manager of the repository
	 */
	@PersistenceContext
	private EntityManager em;

	@Override
	public void checkLoginUser(User user) {
		QUser qUsuario = QUser.user;
		String email = user.getEmail();
		Long id = user.getId();

		BooleanBuilder builder = new BooleanBuilder();
		if (!StringUtils.isEmpty(email)) {
			builder.and(qUsuario.email.equalsIgnoreCase(email));
		}
		if (id != null) {
			builder.and(qUsuario.id.ne(id));
		}

		final JPQLQuery<User> jpaQuery = new JPAQuery(em).from(qUsuario);
		jpaQuery.where(builder);

		long total = jpaQuery.fetchCount();

		if (total > 0) {
			throw new EmailAlreadyRegisteredException();
		}

	}

}
