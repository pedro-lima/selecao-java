package br.com.indra.selecao.pedrolima.api.exceptions.handler;

import java.util.Arrays;
import java.util.List;

import org.springframework.http.HttpStatus;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ApiError {

	private HttpStatus status;
	private String resume;
	private List<String> errors;

	public ApiError(final HttpStatus status, final String resume, final String messagemErro) {
		super();
		this.status = status;
		this.resume = resume;
		this.errors = Arrays.asList(messagemErro);
	}

}
