package br.com.indra.selecao.pedrolima.api.batch;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.listener.JobExecutionListenerSupport;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import br.com.indra.selecao.pedrolima.api.models.CommercialOperation;

@Component
public class CustomJobExecutionListenerSupport extends JobExecutionListenerSupport {

	private static final Logger log = LoggerFactory.getLogger(CustomJobExecutionListenerSupport.class);

	private final JdbcTemplate jdbcTemplate;

	public CustomJobExecutionListenerSupport(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	@Override
	@SuppressWarnings("all")
	public void afterJob(JobExecution jobExecution) {
		if (jobExecution.getStatus() == BatchStatus.COMPLETED) {
			log.info("!!! JOB FINISHED! Time to verify the results");

			List<CommercialOperation> operations = jdbcTemplate.query("select * from t_commercial_operation",
					new CustomRowMapper());

			operations.forEach((CommercialOperation c) -> log.info("Found <" + c.toString() + "> in the database."));
		}

		if (Arrays.asList(BatchStatus.COMPLETED, BatchStatus.FAILED, BatchStatus.STOPPED)
				.contains(jobExecution.getStatus())) {
			FilePathContext.clear();
		}
	}

	private static class CustomRowMapper implements RowMapper<CommercialOperation> {

		@Override
		public CommercialOperation mapRow(ResultSet rs, int rowNum) throws SQLException {
			CommercialOperation operation = new CommercialOperation();

			operation.setId(rs.getLong("id"));
			operation.setFlag(rs.getString("flag"));
			operation.setInstallation(rs.getString("installation"));
			operation.setRegistrationDate(rs.getDate("registration_date").toLocalDate());

			operation.setItem(new CommercialOperation.Item());
			operation.getItem().setPurchasePrice((Double) rs.getObject("purchase_price"));
			operation.getItem().setSaleValue((Double) rs.getObject("sale_value"));

			operation.setProduct(new CommercialOperation.Product());
			operation.getProduct().setCode((Long) rs.getObject("code"));
			operation.getProduct().setProduct(rs.getString("product"));
			operation.getProduct().setUnitMeasurement(rs.getString("unit_measurement"));

			operation.setLocation(new CommercialOperation.Location());
			operation.getLocation().setCity(rs.getString("city"));
			operation.getLocation().setRegion(rs.getString("region"));
			operation.getLocation().setState(rs.getString("state"));

			return operation;
		}

	}

}